CREATE TABLE Country (
    idCountry NUMBER(12, 0),
    name VARCHAR2(50 BYTE) NOT NULL,
    CONSTRAINT ContryPk PRIMARY KEY (idCountry)
);

COMMENT ON COLUMN Country.idCountry IS 'Id del país';
COMMENT ON COLUMN Country.name IS 'Nombre del país';

CREATE TABLE Region (
    idRegion NUMBER(12, 0),
    name VARCHAR2(50 BYTE) NOT NULL,
    idCountry NUMBER(12, 0) NOT NULL,
    CONSTRAINT RegionPk PRIMARY KEY (idRegion),
    CONSTRAINT RegionCountryFk FOREIGN KEY (idCountry) REFERENCES Country (idCountry) ENABLE
);

COMMENT ON COLUMN Region.idRegion IS 'Id de la región';
COMMENT ON COLUMN Region.name IS 'Nombre de la región';
COMMENT ON COLUMN Region.idCountry IS 'Id del país de esa región';

CREATE TABLE Seller (
    identification VARCHAR2(50 BYTE) NOT NULL,
    typeDocument VARCHAR2(3 BYTE) NOT NULL,
    name VARCHAR2(50 BYTE) NOT NULL,
    nickname VARCHAR2(128 BYTE) NOT NULL,
    email VARCHAR2(50 BYTE) NOT NULL,
    phone NUMBER(10, 0) NOT NULL,
    gender VARCHAR2(50 BYTE) NOT NULL,
    birthdate DATE NOT NULL,
    contractDate DATE NOT NULL,
    state VARCHAR2(50 BYTE) NOT NULL,
    idRegion NUMBER(12, 0) NOT NULL,
    identificationFk VARCHAR2(50 BYTE) NULL,
    typeDocumentFk VARCHAR2(3 BYTE) NULL,
    CONSTRAINT SellerPk PRIMARY KEY (identification, typeDocument),    
    CONSTRAINT SellerRegionFk FOREIGN KEY (idRegion) REFERENCES Region (idRegion) ENABLE,
    CONSTRAINT SellerSellerFk FOREIGN KEY (identificationFk, typeDocumentFk) REFERENCES Seller (identification, typeDocument) ENABLE,
    CONSTRAINT GenreCHK CHECK (gender IN ('F', 'M')) ENABLE,
    CONSTRAINT StateSellerCHK CHECK (state IN ('ENABLED', 'DISABLED')) ENABLE
);

COMMENT ON COLUMN Seller.identification IS 'Identificación del vendedor';
COMMENT ON COLUMN Seller.typeDocument IS 'Tipo de documento del vendedor';
COMMENT ON COLUMN Seller.name IS 'Nombre del vendedor';
COMMENT ON COLUMN Seller.nickname IS 'Nickname del vendedor';
COMMENT ON COLUMN Seller.email IS 'Email del vendedor';
COMMENT ON COLUMN Seller.phone IS 'Número teléfonico del vendedor';
COMMENT ON COLUMN Seller.gender IS 'Género del vendedor (F=Femenimo,M=Masculino)';
COMMENT ON COLUMN Seller.birthdate IS 'Fecha de nacimiento del vendedor';
COMMENT ON COLUMN Seller.contractDate IS 'Fecha de contratación del vendedor';
COMMENT ON COLUMN Seller.state IS 'Estado del vendedor (ENABLED,DISABLED)';
COMMENT ON COLUMN Seller.idRegion IS 'Id de la región del vendedor';
COMMENT ON COLUMN Seller.identificationFk IS 'Identificación del jefe';
COMMENT ON COLUMN Seller.typeDocumentFk IS 'Tipo de documento del jefe';

CREATE TABLE Customer (
    identification VARCHAR2(50 BYTE) NOT NULL,
    typeDocument VARCHAR2(3 BYTE) NOT NULL,
    name VARCHAR2(50 BYTE) NOT NULL,
    nickname VARCHAR2(128 BYTE) NOT NULL,
    address VARCHAR2(50 BYTE) NOT NULL,
    city VARCHAR2(50 BYTE) NOT NULL,
    email VARCHAR2(50 BYTE) NOT NULL,
    phone NUMBER(10, 0) NOT NULL,
    state VARCHAR2(50 BYTE) NOT NULL,
    identificationFk VARCHAR2(50 BYTE) NOT NULL,
    typeDocumentFk VARCHAR2(3 BYTE) NOT NULL,
    CONSTRAINT CustomerPk PRIMARY KEY (identification, typeDocument) ENABLE,
    CONSTRAINT CustomerSellerFk FOREIGN KEY (identificationFk, typeDocumentFk) REFERENCES Seller (identification, typeDocument) ENABLE,
    CONSTRAINT TypeDocumentCheck CHECK (typeDocument IN ('C.C', 'T.I', 'C.E')) ENABLE,    
    CONSTRAINT StateCustomerCHK CHECK (state IN ('ENABLED', 'DISABLED')) ENABLE
);

COMMENT ON COLUMN Customer.identification IS 'Identificación del cliente';
COMMENT ON COLUMN Customer.typeDocument IS 'Tipo de documento del cliente (C.C=Cédula de ciudadania,T.I=Tarjeta de identidad,C.E=Cédula de extranjería)';
COMMENT ON COLUMN Customer.name IS 'Nombre del cliente';
COMMENT ON COLUMN Customer.nickname IS 'Nickname del cliente';
COMMENT ON COLUMN Customer.address IS 'Dirección del cliente';
COMMENT ON COLUMN Customer.city IS 'Ciudad del cliente';
COMMENT ON COLUMN Customer.email IS 'Email del cliente';
COMMENT ON COLUMN Customer.phone IS 'Número télefonico del cliente';
COMMENT ON COLUMN Customer.state IS 'Estado del cliente (ENABLED,DISABLED)';
COMMENT ON COLUMN Customer.identificationFk IS 'Identificación del seller';
COMMENT ON COLUMN Customer.typeDocumentFk IS 'Tipo de documento del seller';


CREATE TABLE Season (
    idSeason NUMBER(12, 0),
    levelName VARCHAR2(50 BYTE) NOT NULL,
    minSales NUMBER(12, 0) NOT NULL,
    minQualification NUMBER(12, 0) NOT NULL,
    startDate DATE NOT NULL,
    endDate DATE NOT NULL,
    idSeasonFk NUMBER(12, 0),
    CONSTRAINT SeasonPk PRIMARY KEY (idSeason) ENABLE,
    CONSTRAINT SeasonNextFk FOREIGN KEY (idSeasonFk) REFERENCES Season (idSeason) ENABLE,
    CONSTRAINT SeasonUk UNIQUE (idSeason, startDate, endDate) ENABLE,
    CONSTRAINT endDateCH CHECK (endDate>startDate) ENABLE
);

COMMENT ON COLUMN Season.idSeason IS 'Id del nivel del empleado';
COMMENT ON COLUMN Season.levelName IS 'Nombre del nivel del empleado';
COMMENT ON COLUMN Season.minSales IS 'Número de ventas para ese nivel del empleado';
COMMENT ON COLUMN Season.minQualification IS 'Calificación minima para ese nivel del empleado';
COMMENT ON COLUMN Season.startDate IS 'Fecha de inicio de temporada';
COMMENT ON COLUMN Season.endDate IS 'Fecha final de temporada';
COMMENT ON COLUMN Season.idSeason IS 'Id del siguiente nivel del empleado';

CREATE TABLE SellerSeason (
    idSellerSeason NUMBER(12, 0),
    wholeScore NUMBER(12, 0) NOT NULL,
    wholeSale NUMBER(12, 0) NOT NULL,
    idSeason NUMBER(12, 0) NOT NULL,
    identification VARCHAR2(50 BYTE) NOT NULL,
    typeDocument VARCHAR2(3 BYTE) NOT NULL,
    CONSTRAINT SellerSeasonPk PRIMARY KEY (idSellerSeason) ENABLE,
    CONSTRAINT SeasonFk FOREIGN KEY (idSeason) REFERENCES Season (idSeason) ENABLE,
    CONSTRAINT SeallerFk FOREIGN KEY (identification, typeDocument) REFERENCES Seller (identification, typeDocument) ENABLE
);

CREATE TABLE Delivery (
    idDelivery NUMBER(12, 0),
    dateDelivered DATE NOT NULL,
    total NUMBER(12, 0) NOT NULL,
    score NUMBER(12, 0),
    state VARCHAR2(50 BYTE) NOT NULL,    
    identificationCustomer VARCHAR2(50 BYTE) NOT NULL,
    typeDocumentCustomer VARCHAR2(3 BYTE) NOT NULL,
    identificationSeller VARCHAR2(50 BYTE) NOT NULL,
    typeDocumentSeller VARCHAR2(3 BYTE) NOT NULL,
    CONSTRAINT DeliveryPk PRIMARY KEY (idDelivery) ENABLE,
    CONSTRAINT CustomerFk FOREIGN KEY (identificationCustomer, typeDocumentCustomer) REFERENCES Customer (identification, typeDocument) ENABLE,
    CONSTRAINT SellerFk FOREIGN KEY (identificationSeller, typeDocumentSeller) REFERENCES Seller (identification, typeDocument) ENABLE,
    CONSTRAINT DeliveryUnique UNIQUE (idDelivery, identificationCustomer, typeDocumentCustomer, identificationSeller, typeDocumentSeller) ENABLE,
    CONSTRAINT StateDeliveryCHK CHECK (state IN ('PROCESS', 'FINISHED', 'DELETED')) ENABLE
);

COMMENT ON COLUMN Delivery.idDelivery IS 'Id del pedido';
COMMENT ON COLUMN Delivery.dateDelivered IS 'Fecha de toma del pedido';
COMMENT ON COLUMN Delivery.total IS 'Total de los pedidos realizados';
COMMENT ON COLUMN Delivery.score IS 'Puntuación del pedido';
COMMENT ON COLUMN Delivery.state IS 'Estado del pedido (PROCESS,FINISHED,DELETED)';
COMMENT ON COLUMN Delivery.identificationCustomer IS 'Número de identificacion del cliente que realizo el pedido';
COMMENT ON COLUMN Delivery.typeDocumentCustomer IS 'Tipo de identificacion del cliente que realizo el pedido';
COMMENT ON COLUMN Delivery.identificationSeller IS 'Número de identificacion del rep. de ventas que realizo el pedido';
COMMENT ON COLUMN Delivery.typeDocumentSeller IS 'Tipo de identificacion del rep. de ventas que realizo el pedido';

CREATE TABLE Category (
    idCategory NUMBER(12, 0),
    name VARCHAR2(50 BYTE) NOT NULL,
    idParentCategory NUMBER(12, 0),
    CONSTRAINT CategoryPk PRIMARY KEY (idCategory) ENABLE,
    CONSTRAINT CategoryParentCategoryFk FOREIGN KEY (idParentCategory) REFERENCES Category (idCategory) ENABLE
);

COMMENT ON COLUMN Category.idCategory IS 'Id de la categoría';
COMMENT ON COLUMN Category.name IS 'Nombre de la categoría';
COMMENT ON COLUMN Category.idParentCategory IS 'Id de la categoría padre';
COMMENT ON COLUMN Category.idCategory IS 'Id de la categoría';

CREATE TABLE Product (
    idProduct NUMBER(12, 0),
    name VARCHAR2(50 BYTE) NOT NULL,
    idCategory NUMBER(12, 0) NOT NULL,
    CONSTRAINT ProductPK PRIMARY KEY (idProduct) ENABLE,
    CONSTRAINT ProductCategoryFk FOREIGN KEY (idCategory) REFERENCES Category (idCategory) ENABLE
);

COMMENT ON COLUMN Product.idProduct IS 'Id del producto';
COMMENT ON COLUMN Product.name IS 'Nombre del producto';
COMMENT ON COLUMN Product.idCategory IS 'Id de la categoría del producto';

CREATE TABLE ProductRegion (
    idProductRegion NUMBER(12, 0),
    stock NUMBER(12, 0) NOT NULL,
    price NUMBER(12, 0) NOT NULL,
    idProduct NUMBER(12, 0) NOT NULL,
    idRegion NUMBER(12, 0) NOT NULL,
    CONSTRAINT ProductRegionPk PRIMARY KEY (idProductRegion) ENABLE,
    CONSTRAINT ProductRegionProductFk FOREIGN KEY (idProduct) REFERENCES Product (idProduct) ENABLE,
    CONSTRAINT ProductRegionRegionFk FOREIGN KEY (idRegion) REFERENCES Region (idRegion) ENABLE,
    CONSTRAINT ProductRegionUnique UNIQUE (idProduct, idRegion)
);

CREATE TABLE ProductRegionDelivery (
    idProductRegionDelivery NUMBER(12, 0),
    idProductRegion NUMBER(12, 0) NOT NULL,
    idDelivery NUMBER(12, 0) NOT NULL,
    quantity NUMBER(12, 0) DEFAULT 0 NOT NULL,
    CONSTRAINT ProductRegionDeliveryPk PRIMARY KEY (idProductRegionDelivery) ENABLE,
    CONSTRAINT ProductRegionFk FOREIGN KEY (idProductRegion) REFERENCES ProductRegion (idProductRegion) ENABLE,
    CONSTRAINT DeliveryFk FOREIGN KEY (idDelivery) REFERENCES Delivery (idDelivery) ENABLE,
    CONSTRAINT ProductRegionDeliveryUnique UNIQUE (idProductRegion, idDelivery) ENABLE
);

CREATE TABLE AuditStock (
    idAditoryStock NUMBER(12, 0) NOT NULL, 
    dateUpdated DATE NOT NULL, 
    nickname VARCHAR2(10 BYTE) NOT NULL,
    idProductRegion NUMBER(12, 0) NOT NULL,
    oldStock NUMBER(12,0),
    newStock NUMBER(12,0),
    CONSTRAINT AuditStockPk PRIMARY KEY (idAditoryStock) ENABLE,
    CONSTRAINT ProductRegionAudit FOREIGN KEY (idProductRegion) REFERENCES ProductRegion (idProductRegion) ENABLE
);

CREATE SEQUENCE SEQ_AUDIT_STOCK
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_PRODUCT
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_CATEGORY
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_COUNTRY
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_REGION
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_SEASON
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_SELLER_SEASON
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_DELIVERY
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_PRODUCT_REGION_DELIVERY
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;

CREATE SEQUENCE SEQ_PRODUCT_REGION
    START WITH 1
    INCREMENT BY 1
    MAXVALUE 999999999999999999999999999
    CYCLE
    CACHE 20;
    
