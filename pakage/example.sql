SET SERVEROUTPUT ON;
DECLARE
    v_iddelivery delivery.iddelivery%TYPE := 17; -- Reemplaza con el iddelivery deseado
    v_cursor SYS_REFCURSOR;
    v_idcustomer VARCHAR2(50);
    v_idseller VARCHAR2(50);
    v_delivery_date VARCHAR2(50);
    v_total VARCHAR2(50);
    v_subtotal VARCHAR2(50);
    v_quantity NUMBER;
    v_price NUMBER;
    v_product_name VARCHAR2(50);
    pc_error NUMBER;
    pm_error VARCHAR2(50);
BEGIN
    -- Llamar al procedimiento generar_factura
    pk_natame.generar_factura(v_iddelivery, v_cursor, v_idcustomer, v_idseller, v_delivery_date, v_total,v_subtotal,pc_error,pm_error);

    -- Imprimir la factura
    DBMS_OUTPUT.PUT_LINE('Factura de Venta');
    DBMS_OUTPUT.PUT_LINE('----------------');
    DBMS_OUTPUT.PUT_LINE('Cliente: ' || v_idcustomer);
    DBMS_OUTPUT.PUT_LINE('Vendedor: ' || v_idseller);
    DBMS_OUTPUT.PUT_LINE('Fecha de Entrega: ' || v_delivery_date);
    DBMS_OUTPUT.PUT_LINE('Total: ' || v_total);
    DBMS_OUTPUT.PUT_LINE('Total: ' || v_subtotal);
    DBMS_OUTPUT.PUT_LINE('Detalles de los Productos:');
    DBMS_OUTPUT.PUT_LINE('-------------------------');
    LOOP
        FETCH v_cursor INTO  v_quantity, v_product_name, v_price;
        EXIT WHEN v_cursor%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE('Nombre: ' || v_product_name ||' Cantidad: ' || v_quantity || ', Precio: ' || v_price);
    END LOOP;

    -- Cerrar el cursor
    CLOSE v_cursor;
END;
/