CREATE OR REPLACE PACKAGE BODY PK_NATAME IS
/*-----------------------------------------------------------------------------------------------------------
  Incluye la implementación de los procedimiento y funciones asociados a la gestión de estudiantes del Sistema de 
  Gestión Académica.
  Autor: Alba Consuelo Nieto
  Fecha de creación : yyyy/mm/dd
  Fecha de modificación: yyyy/mm/dd
*/
     /*------------------------------------------------------------------------------
     Procedimiento para buscar un estudiante
     Parametros de Entrada: pk_codest       Código del estudiante a buscar
     Parametros de Salida : pr_estudiante   Registro con los datos básicos del estudiante
                            pm_error        Mensaje de error si hay error o null en caso contrario
                            pc_error        = 1 si no existe,
                                              0, en caso contrario
    */
        PROCEDURE totalizar_carrito(
            p_iddelivery IN root.delivery.iddelivery%TYPE,
            p_total OUT NUMBER,
            p_total_iva OUT NUMBER
        ) AS
            l_subtotal NUMBER;
            l_iva_percentage NUMBER := 0.16; -- Porcentaje del IVA
        BEGIN
            -- Calcular el subtotal sumando el precio de los productos en el carrito
            SELECT SUM(pr.price * prd.quantity)
            INTO l_subtotal
            FROM root.productregiondelivery prd, root.productregion pr
            WHERE prd.iddelivery = p_iddelivery and prd.idproductregion = pr.idproductregion;        
            -- Calcular el total sumando el subtotal y el IVA
            p_total := l_subtotal;
            p_total_iva := l_subtotal + (l_subtotal * l_iva_percentage);
        END totalizar_carrito;




        /*---------------------------------------------------------------------------
        Función para retornar los datos de un grupo dado
        Parámetros de entrada: pn_nombre               Nombre de la asignatura
                                pq_anio                 Año
                                pq_periodo              Periodo académico
                                pn_grupo                Nombre del Grupo
        Parámetros de salida:  pm_error                Mensaje de error si no existe el grupo o hay una excepción, null en caso contrario
                                pc_error       = 1 No existe grupo o hay una excepción
                                                0 En caso contrario
        Retorna             : Registro con los datos del grupo
        */

    PROCEDURE calificar_vendedor (
        p_identificationcustomer root.customer.identification%TYPE,
        p_typedocumentcustomer IN root.customer.typedocument%TYPE,
        p_identificationseller IN root.seller.identification%TYPE,
        p_typedocumentseller IN root.seller.typedocument%TYPE,
        p_score IN root.delivery.score%TYPE,
        p_iddelivery IN root.delivery.iddelivery%TYPE
    ) IS
        v_customer_exists NUMBER;
        v_seller_exists NUMBER;
    BEGIN
        -- Verificar si el cliente y el vendedor existen
        
            
        
            SELECT COUNT(*) INTO v_customer_exists
            FROM root.customer
            WHERE identification = p_identificationcustomer AND typedocument = p_typedocumentcustomer;
            
            SELECT COUNT(*) INTO v_seller_exists
            FROM root.seller
            WHERE identification = p_identificationseller AND typedocument = p_typedocumentseller;
            
            IF v_customer_exists = 0 THEN
                RAISE_APPLICATION_ERROR(-20001, 'El cliente no existe.');
            ELSIF v_seller_exists = 0 THEN
                RAISE_APPLICATION_ERROR(-20002, 'El vendedor no existe.');            
            END IF;
            UPDATE root.delivery
            SET score = p_score
            WHERE iddelivery = p_iddelivery;
            commit;            
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20003, 'Error: No se encontraron registros en la base de datos.');
            WHEN OTHERS THEN
                RAISE_APPLICATION_ERROR(-20004, 'Error: ' || SQLERRM);

    END calificar_vendedor;

    PROCEDURE generar_factura (
        p_iddelivery IN delivery.iddelivery%TYPE,
        p_cursor OUT SYS_REFCURSOR,
        p_idcustomer OUT VARCHAR2,
        p_idseller OUT VARCHAR2,
        p_delivery_date OUT VARCHAR2,
        p_total OUT VARCHAR2,
        p_subtotal OUT VARCHAR2,
        pc_error OUT NUMBER,
        pm_error OUT VARCHAR2
    ) IS

    BEGIN
        pc_error := 0;
        pm_error := '';
        -- Obtener la información del cliente y vendedor
        SELECT identificationcustomer, identificationseller, TO_CHAR(datedelivered, 'DD-MM-YYYY')
        INTO p_idcustomer, p_idseller, p_delivery_date
        FROM delivery
        WHERE iddelivery = p_iddelivery;
         pk_natame.totalizar_carrito(
            p_iddelivery,
            p_subtotal,
            p_total 
        );
        -- Obtener los detalles de los productos
        OPEN p_cursor FOR
            SELECT prd.quantity, p.name, pr.price
            FROM productregiondelivery prd
            JOIN productregion pr ON prd.idproductregion = pr.idproductregion
            JOIN product p ON pr.idproduct = p.idproduct
            WHERE prd.iddelivery = p_iddelivery;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            pc_error := 1;
            pm_error := 'No se encontraron detalles de productos para la entrega con el ID ' || p_iddelivery;
            
        WHEN OTHERS THEN
            pc_error := 1;
            pm_error := SQLERRM;            
    END generar_factura;



    


    END PK_NATAME;
    /