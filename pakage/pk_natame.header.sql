CREATE OR REPLACE PACKAGE PK_NATAME IS
/*-----------------------------------------------------------------------------------------------------------
  Incluye la definición de los procedimiento y funciones asociados a la gestion de NATAME
  Autor: 
    - DAVID FELIPE VEGA
    - CRISTIAN MARTINEZ
    - JUAN PARDO
    - GABRIEL PARRA
    - OSCAR GUTIERREZ
  Fecha de creación: 2023/05/14
  Fecha de modificación:  2023/05/20
*/
    /*------------------------------------------------------------------------------
     Procedimiento para buscar totalizar carrito incluyendo el calculo de IVA
     Parametros de Entrada: pk_codest       Código del estudiante a buscar
     Parametros de Salida : pr_estudiante   Registro con los datos básicos del estudiante
                            pm_error        Mensaje de error si hay error o null en caso contrario
                            pc_error        = 1 si no existe,
                                              0, en caso contrario
    */
    PROCEDURE totalizar_carrito(
        p_iddelivery IN root.delivery.iddelivery%TYPE,
        p_total OUT NUMBER,
        p_total_iva OUT NUMBER
    );
    /*------------------------------------------------------------------------------
     Procedimiento para buscar totalizar carrito incluyendo el calculo de IVA
     Parametros de Entrada: pk_codest       Código del estudiante a buscar
     Parametros de Salida : pr_estudiante   Registro con los datos básicos del estudiante
                            pm_error        Mensaje de error si hay error o null en caso contrario
                            pc_error        = 1 si no existe,
                                              0, en caso contrario
    */
    PROCEDURE calificar_vendedor (
        p_identificationcustomer root.customer.identification%TYPE,
        p_typedocumentcustomer IN root.customer.typedocument%TYPE,
        p_identificationseller IN root.seller.identification%TYPE,
        p_typedocumentseller IN root.seller.typedocument%TYPE,
        p_score IN root.delivery.score%TYPE,
        p_iddelivery IN root.delivery.iddelivery%TYPE
    );
    /*------------------------------------------------------------------------------
     Procedimiento para buscar totalizar carrito incluyendo el calculo de IVA
     Parametros de Entrada: pk_codest       Código del estudiante a buscar
     Parametros de Salida : pr_estudiante   Registro con los datos básicos del estudiante
                            pm_error        Mensaje de error si hay error o null en caso contrario
                            pc_error        = 1 si no existe,
                                              0, en caso contrario
    */
    PROCEDURE generar_factura (
        p_iddelivery IN delivery.iddelivery%TYPE,
        p_cursor OUT SYS_REFCURSOR,
        p_idcustomer OUT VARCHAR2,
        p_idseller OUT VARCHAR2,
        p_delivery_date OUT VARCHAR2,
        p_total OUT VARCHAR2,
        p_subtotal Out VARCHAR2,
        pc_error OUT NUMBER,
        pm_error OUT VARCHAR2
    );

END PK_NATAME;
/