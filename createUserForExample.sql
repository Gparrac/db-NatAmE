create user sellerexample identified by sellerexample 
default tablespace NATAMEDEF temporary tablespace NATAMETMP 
quota 2M on NATAMEDEF;
grant RSeller to sellerexample;

create user masterexample identified by masterexample 
default tablespace NATAMEDEF temporary tablespace NATAMETMP 
quota 2M on NATAMEDEF;
grant RMasterSeller to masterexample;

create user customerexample identified by customerexample 
default tablespace NATAMEDEF temporary tablespace NATAMETMP 
quota 2M on NATAMEDEF;
grant RCustomer to customerexample;

create user managerexample identified by managerexample 
default tablespace NATAMEDEF temporary tablespace NATAMETMP 
quota 2M on NATAMEDEF;
grant RManagerRegion to managerexample;