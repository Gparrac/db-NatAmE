INSERT INTO root.CATEGORY (IDCATEGORY, NAME, IDPARENTCATEGORY) VALUES (root.seq_category.nextval, 'Cuidado Personal', NULL);
INSERT INTO root.CATEGORY (IDCATEGORY, NAME, IDPARENTCATEGORY) VALUES (root.seq_category.nextval, 'Limpieza del Hogar', NULL);
INSERT INTO root.CATEGORY (IDCATEGORY, NAME, IDPARENTCATEGORY) VALUES (root.seq_category.nextval, 'Cosméticos', 3);
INSERT INTO root.CATEGORY (IDCATEGORY, NAME, IDPARENTCATEGORY) VALUES (root.seq_category.nextval, 'Cuidado del Cabello', 3);
INSERT INTO root.CATEGORY (IDCATEGORY, NAME, IDPARENTCATEGORY) VALUES (root.seq_category.nextval, 'Jabones', 4);

-- Insertar productos
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Jabón Natural', 23);
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Champú Orgánico', 22);
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Crema Hidratante', 3);
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Gel de Ducha', 3);
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Desodorante Natural', 21);
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Limpiador Multiusos Ecológico', 4);
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Crema Facial Antiedad', 21);
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Acondicionador Natural', 3);
INSERT INTO root.PRODUCT (IDPRODUCT, NAME, IDCATEGORY) VALUES (root.seq_product.nextval, 'Jabón de Manos Antibacterial', 23);