/*
 Previously should login with 'system' and create the pdb with the next features:
 nameDb: natame
 user: admin
 pwdUser:admin 
 */
/*
 If you required to drop tablespace or users, you could use these:
 
 tablespace: DROP TABLESPACE NATAMETMP INCLUDING CONTENTS AND DATAFILES;
 user: DROP USER MASTER CASCADE;
 */
create tablespace NATAMEDEF 
-- datafile 'C:\Oracle\Database21c\oradata\ORCL\natame\NATAMEDEF.DBF' 
datafile 'C:\Oracle\db19c\oradata\ORCL\natame\NATAMEDEF.DBF' 
size 2M autoextend on;

create temporary tablespace NATAMETMP 
-- datafile 'C:\Oracle\Database21c\oradata\ORCL\natame\NATAMETMP.DBF' 
tempfile 'C:\Oracle\db19c\oradata\ORCL\natame\NATAMETMP.DBF' 
size 2M autoextend on;

create user root identified by root 
default tablespace NATAMEDEF temporary tablespace NATAMETMP 
quota 2M on NATAMEDEF;

grant connect, resource to root;
grant grant any role to root;
