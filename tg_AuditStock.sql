CREATE OR REPLACE TRIGGER TG_AUDIT_STOCK
AFTER UPDATE OF stock ON ProductRegion

FOR EACH ROW

BEGIN
    INSERT INTO AuditStock (idAditoryStock, dateUpdated, nickname, idProductRegion, oldStock, newStock)
    VALUES (SEQ_AUDIT_STOCK.NEXTVAL, SYSDATE, USER, :NEW.idProductRegion, :OLD.stock, :NEW.stock);
END TG_AUDIT_STOCK;
/